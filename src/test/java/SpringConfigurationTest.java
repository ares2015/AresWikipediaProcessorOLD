import com.aresWikipediaProcessor.configuration.SpringConfiguration;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringConfigurationTest {

    @Test
    public void testConfiguration() {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        try {
            ctx.register(SpringConfiguration.class);
            ctx.refresh();
            SpringConfiguration config = (SpringConfiguration) ctx.getBean("springConfiguration");
            System.out.println(config);
        } finally {
            ctx.close();
        }
    }

}