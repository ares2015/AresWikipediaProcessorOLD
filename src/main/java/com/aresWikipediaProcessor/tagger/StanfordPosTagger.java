package com.aresWikipediaProcessor.tagger;


import com.aresWikipediaProcessor.cache.ConstantWordsCache;
import com.aresWikipediaProcessor.cache.TagsConversionCache;
import com.aresWikipediaProcessor.morphology.NumberPrefixDetector;
import com.aresWikipediaProcessor.morphology.NumberPrefixDetectorImpl;
import com.aresWikipediaProcessor.tags.StanfordTags;
import com.aresWikipediaProcessor.tags.Tags;
import com.aresWikipediaProcessor.tokenizing.Tokenizer;
import com.aresWikipediaProcessor.tokenizing.TokenizerImpl;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

@Component
public class StanfordPosTagger {

    private final Properties props;

    private final StanfordCoreNLP pipeline;

    private final Tokenizer tokenizer = new TokenizerImpl();

    private final NumberPrefixDetector numberPrefixDetector = new NumberPrefixDetectorImpl();

    public StanfordPosTagger() {
        props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, pos");
        pipeline = new StanfordCoreNLP(props);
    }

    public List<String> tag(String inputSentence) {
        List<String> tagSequences = new ArrayList<>();
        List<String> tokens = tokenizer.getTokens(inputSentence);
        Set<Integer> commaIndexes = tokenizer.getCommaIndexes(tokens);
        int sentenceLength = tokens.size();
        StringBuilder stringBuilder = new StringBuilder();
        Annotation annotation = new Annotation(inputSentence);
        pipeline.annotate(annotation);
        if (annotation.get(CoreAnnotations.SentencesAnnotation.class).size() > 0) {
            CoreMap processedSentence = annotation.get(CoreAnnotations.SentencesAnnotation.class).get(0);
            int index = 0;
            for (CoreLabel stfToken : processedSentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = stfToken.get(CoreAnnotations.TextAnnotation.class);
                String tag = stfToken.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                if (isStandardWord(word, tag)) {
                    if (numberPrefixDetector.detect(word)) {
                        stringBuilder.append(Tags.NUMBER);
                    } else {
                        if (isCapitalizedNoun(index, word, tag)) {
                            stringBuilder.append(Tags.NOUN);
                        } else {
                            String token = tokens.get(index);
                            if (token.contains(",") || token.contains(".")) {
                                token = tokenizer.removeCommaAndDot(token);
                            }
                            if (isConstantWord(token)) {
                                stringBuilder.append(ConstantWordsCache.constantWordsCacheMap.get(tokenizer.decapitalize(token)));
                            } else {
                                stringBuilder.append(TagsConversionCache.cache.get(tag));
                            }
                        }
                    }
                    if (commaIndexes.contains(index)) {
                        tagSequences.add(stringBuilder.toString());
                        stringBuilder.setLength(0);
                    }
                    if (index <= sentenceLength - 1) {
                        stringBuilder.append(" ");
                    }
                    index++;
                }
            }
            tagSequences.add(stringBuilder.toString());
        }
        return tagSequences;
    }

    private boolean isConstantWord(String token) {
        return ConstantWordsCache.constantWordsCacheMap.containsKey(tokenizer.decapitalize(token));
    }

    private boolean isCapitalizedNoun(int index, String word, String tag) {
        return index > 0 && !Tags.PRONOUN_PERSONAL.equals(tag) && Character.isUpperCase(word.charAt(0));
    }

    private boolean isStandardWord(String word, String tag) {
        return !",".equals(tag) && !".".equals(tag) && !StanfordTags.POSSESIVE_ENDING.equals(tag) && !"n't".equals(word);
    }

}