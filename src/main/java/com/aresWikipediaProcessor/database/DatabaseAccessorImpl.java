package com.aresWikipediaProcessor.database;

import com.aresChunker.data.ChunkData;
import com.aresWikipediaProcessor.data.WikipediaExtractionData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Component
public class DatabaseAccessorImpl implements DatabaseAccessor {

    private final static Logger LOGGER = Logger.getLogger(DatabaseAccessorImpl.class.getName());

    @Autowired
    private DriverManagerDataSource driverManagerDataSource;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTemplate standardJdbcTemplate;

    private final String WIKIPEDIA_EXTRACTION_TABLE = "jos_ares_wikipedia_extraction_history_politics_economy";

    private final String WIKIPEDIA_CHUNKS_TABLE = "jos_ares_wikipedia_chunks_history_politics_economy";

    @Override
    public void insertWikipediaExtractionDataWithBatch(List<WikipediaExtractionData> wikipediaExtractionDataList) {
        String query = "INSERT INTO  " + WIKIPEDIA_EXTRACTION_TABLE + " (subject, verb_predicate, noun_predicate, sentence, title) VALUES (?,?,?,?,?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
        List<WikipediaExtractionData> filteredData = new ArrayList<>();

        for (WikipediaExtractionData wikipediaExtractionData : wikipediaExtractionDataList) {
            Optional<ChunkData> optionalChunkData = getChunkData(wikipediaExtractionData);
            if (optionalChunkData.isPresent()) {
                ChunkData chunkData = optionalChunkData.get();
                wikipediaExtractionData.setDbSubject(chunkData.getSubject());
                wikipediaExtractionData.setDbVerbRelation(chunkData.getVerbRelation());
                wikipediaExtractionData.setDbObject(chunkData.getObject());
                filteredData.add(wikipediaExtractionData);
            }
        }


        LOGGER.info("INSERTING " + filteredData.size() + " rows of CHUNK DATA INTO " + WIKIPEDIA_EXTRACTION_TABLE);
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");

        jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, filteredData.get(i).getDbSubject());
                ps.setString(2, filteredData.get(i).getDbVerbRelation());
                ps.setString(3, filteredData.get(i).getDbObject());
                ps.setString(4, filteredData.get(i).getSentence());
                ps.setString(5, filteredData.get(i).getTitle());
            }

            @Override
            public int getBatchSize() {
                return filteredData.size();
            }
        });

    }

    @Override
    public void insertChunkDataWithBatch(List<WikipediaExtractionData> wikipediaExtractionDataList) {
        String query = "INSERT INTO  " + WIKIPEDIA_CHUNKS_TABLE + " (subject, verb_predicate, noun_predicate, sentence, title) VALUES (?,?,?,?,?)";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);

        List<WikipediaExtractionData> chunkDataList = new ArrayList<>();
        for (WikipediaExtractionData wikipediaExtractionData : wikipediaExtractionDataList) {
            for (ChunkData chunkData : wikipediaExtractionData.getChunkDataList()) {
                if (canBeInsertedToDatabase(chunkData)) {
                    WikipediaExtractionData newChunkData = new WikipediaExtractionData();
                    newChunkData.setDbSubject(chunkData.getSubject());
                    newChunkData.setDbVerbRelation(chunkData.getVerbRelation());
                    newChunkData.setDbObject(chunkData.getObject());
                    newChunkData.setSentence(wikipediaExtractionData.getSentence());
                    newChunkData.setTitle(wikipediaExtractionData.getTitle());
                    newChunkData.setUrl(wikipediaExtractionData.getUrl());
                    chunkDataList.add(newChunkData);
                }
            }
        }

        LOGGER.info("INSERTING " + chunkDataList.size() + " rows of CHUNK DATA INTO " + WIKIPEDIA_CHUNKS_TABLE);
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");
        LOGGER.info("***********************************************");

        jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, chunkDataList.get(i).getDbSubject());
                ps.setString(2, chunkDataList.get(i).getDbVerbRelation());
                ps.setString(3, chunkDataList.get(i).getDbObject());
                ps.setString(4, chunkDataList.get(i).getSentence());
                ps.setString(5, chunkDataList.get(i).getTitle());
            }

            @Override
            public int getBatchSize() {
                return chunkDataList.size();
            }
        });
    }

    private Optional<ChunkData> getChunkData(WikipediaExtractionData wikipediaExtractionData) {
        if (wikipediaExtractionData.getChunkDataList().size() == 1) {
            return Optional.of(wikipediaExtractionData.getChunkDataList().get(0));
        } else {
            for (ChunkData chunkData : wikipediaExtractionData.getChunkDataList()) {
                if (canBeInsertedToDatabase(chunkData)) {
                    return Optional.of(chunkData);
                }
            }
        }
        return Optional.empty();
    }

    private boolean canBeInsertedToDatabase(ChunkData chunkData) {
        return chunkData.getSubject() != null && !"".equals(chunkData.getSubject()) && !" ".equals(chunkData.getSubject()) &&
                chunkData.getVerbRelation() != null && !"".equals(chunkData.getVerbRelation()) && !" ".equals(chunkData.getVerbRelation()) &&
                chunkData.getObject() != null && !"".equals(chunkData.getObject()) && !" ".equals(chunkData.getObject());
    }
    
}