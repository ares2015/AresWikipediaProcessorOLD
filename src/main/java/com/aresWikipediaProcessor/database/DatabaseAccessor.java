package com.aresWikipediaProcessor.database;

import com.aresWikipediaProcessor.data.WikipediaExtractionData;

import java.util.List;

public interface DatabaseAccessor {

    void insertWikipediaExtractionDataWithBatch(List<WikipediaExtractionData> newsProcessingDataList);

    void insertChunkDataWithBatch(List<WikipediaExtractionData> newsProcessingDataList);

}