package com.aresWikipediaProcessor.data;

import java.util.ArrayList;
import java.util.List;

public class InputData {

    private List<String> tokensList = new ArrayList<>();

    private List<String> tagsList = new ArrayList<>();

    public void setTokensList(List<String> tokensList) {
        this.tokensList = tokensList;
    }

    public void setTagsList(List<String> tagsList) {
        this.tagsList = tagsList;
    }

    public List<String> getTokensList() {
        return tokensList;
    }

    public List<String> getTagsList() {
        return tagsList;
    }
}
