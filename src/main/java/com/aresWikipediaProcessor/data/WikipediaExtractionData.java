package com.aresWikipediaProcessor.data;

import com.aresChunker.data.ChunkData;

import java.util.List;

public class WikipediaExtractionData {

    private int id;

    private String dbSubject;

    private String dbVerbRelation;

    private String dbObject;

    private String sentence;

    private String title;

    private String url;

    private List<String> tokensList;

    private List<String> tagsList;

    private List<ChunkData> chunkDataList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDbSubject() {
        return dbSubject;
    }

    public void setDbSubject(String dbSubject) {
        this.dbSubject = dbSubject;
    }

    public String getDbVerbRelation() {
        return dbVerbRelation;
    }

    public void setDbVerbRelation(String dbVerbRelation) {
        this.dbVerbRelation = dbVerbRelation;
    }

    public String getDbObject() {
        return dbObject;
    }

    public void setDbObject(String dbObject) {
        this.dbObject = dbObject;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTokensList() {
        return tokensList;
    }

    public void setTokensList(List<String> tokensList) {
        this.tokensList = tokensList;
    }

    public List<String> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<String> tagsList) {
        this.tagsList = tagsList;
    }

    public List<ChunkData> getChunkDataList() {
        return chunkDataList;
    }

    public void setChunkDataList(List<ChunkData> chunkDataList) {
        this.chunkDataList = chunkDataList;
    }
}