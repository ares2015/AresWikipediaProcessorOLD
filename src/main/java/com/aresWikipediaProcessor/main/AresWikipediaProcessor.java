package com.aresWikipediaProcessor.main;

import com.aresChunker.data.ChunkData;
import com.aresChunker.main.AresChunker;
import com.aresWikipediaProcessor.configuration.SpringConfiguration;
import com.aresWikipediaProcessor.data.InputData;
import com.aresWikipediaProcessor.data.WikipediaExtractionData;
import com.aresWikipediaProcessor.database.DatabaseAccessor;
import com.aresWikipediaProcessor.factories.InputDataFactory;
import com.aresWikipediaProcessor.preprocessing.CapitalizedTokensPreprocessor;
import com.aresWikipediaProcessor.preprocessing.SentencesPreprocessor;
import com.aresWikipediaProcessor.preprocessing.SubSentencesProcessor;
import com.aresWikipediaProcessor.tagger.StanfordPosTagger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AresWikipediaProcessor {


    public static void main(String[] args) throws IOException {

        String title = args[0];

        String url = "https://en.wikipedia.org/wiki/" + title;

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

        ctx.register(SpringConfiguration.class);
        ctx.refresh();
        SpringConfiguration config = (SpringConfiguration) ctx.getBean("springConfiguration");
        SentencesPreprocessor sentencesPreprocessor = config.getSentencesPreprocessor();
        SubSentencesProcessor subSentencesProcessor = config.getSubSentencesProcessor();
        StanfordPosTagger stanfordPosTagger = config.getStanfordPosTagger();
        InputDataFactory inputDataFactory = config.getInputDataFactory();
        CapitalizedTokensPreprocessor capitalizedTokensPreprocessor = config.getCapitalizedTokensPreprocessor();
        DatabaseAccessor databaseAccessor = config.getDatabaseAccessor();
        AresChunker aresChunker = new AresChunker();

        List<WikipediaExtractionData> wikipediaExtractionDataList = new ArrayList<>();
        List<String> sentences = new ArrayList<>();
        Document doc = Jsoup.connect(url).get();
        Elements ps = doc.select("p");
        String rawSentence = ps.text();
        subSentencesProcessor.processSubSentences(sentences, rawSentence);
        for (String sentence : sentences) {
            try {
                String preprocessedSentence = sentencesPreprocessor.preprocess(sentence);
                System.out.println(preprocessedSentence);
                WikipediaExtractionData wikipediaExtractionData = new WikipediaExtractionData();
                wikipediaExtractionData.setSentence(sentence);
                wikipediaExtractionData.setUrl(url);
                wikipediaExtractionData.setTitle(title);

                List<String> tagSequences = stanfordPosTagger.tag(sentence);
                InputData inputData = inputDataFactory.create(sentence, tagSequences);
                capitalizedTokensPreprocessor.process(inputData);
                List<String> tokensList = inputData.getTokensList();
                wikipediaExtractionData.setTokensList(tokensList);
                List<String> tagsList = inputData.getTagsList();
                wikipediaExtractionData.setTagsList(tagsList);
                List<ChunkData> chunkDataList = aresChunker.chunk(tokensList, tagsList);
                wikipediaExtractionData.setChunkDataList(chunkDataList);
                wikipediaExtractionDataList.add(wikipediaExtractionData);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        databaseAccessor.insertWikipediaExtractionDataWithBatch(wikipediaExtractionDataList);
        databaseAccessor.insertChunkDataWithBatch(wikipediaExtractionDataList);

    }


}