package com.aresWikipediaProcessor.preprocessing;

public interface SentencesPreprocessor {

    String preprocess(String sentence);

}
