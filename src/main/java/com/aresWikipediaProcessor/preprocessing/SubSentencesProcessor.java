package com.aresWikipediaProcessor.preprocessing;

import java.util.List;

public interface SubSentencesProcessor {

    void processSubSentences(List<String> sentences, String sentence);

}