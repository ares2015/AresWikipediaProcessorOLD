package com.aresWikipediaProcessor.preprocessing;

import com.aresWikipediaProcessor.data.InputData;

public interface CapitalizedTokensPreprocessor {

    void process(InputData inputData);

}