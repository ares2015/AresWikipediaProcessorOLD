package com.aresWikipediaProcessor.preprocessing;

import com.aresWikipediaProcessor.tokenizing.Tokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SubSentencesProcessorImpl implements SubSentencesProcessor {

    @Autowired
    private Tokenizer tokenizer;

    public void processSubSentences(List<String> sentences, String sentence) {
        List<String> subSentences = tokenizer.getSubsentences(sentence);
        for (String subSentence : subSentences) {
            subSentence = tokenizer.removeSpecialCharactersForPreprocessor(subSentence);
            List<String> tokens = tokenizer.splitStringIntoList(subSentence);
            String filteredSentence = tokenizer.convertTokensListToSentence(tokens);
            sentences.add(filteredSentence);
        }
    }
}
