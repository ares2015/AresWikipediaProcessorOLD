package com.aresWikipediaProcessor.preprocessing;

import com.aresWikipediaProcessor.tokenizing.Tokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SentencePreprocessorImpl implements SentencesPreprocessor {

    @Autowired
    private Tokenizer tokenizer;

    @Override
    public String preprocess(String sentence) {
        String[] tokTmp;
        tokTmp = sentence.split("\\ ");
        List<String> preprocessedTokens = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        boolean isBracketSequence = false;
        for (String token : tokTmp) {
            if (token.startsWith("(") || token.startsWith("[")) {
                isBracketSequence = true;
            }
            if (isBracketSequence && (token.endsWith(")") || isBracketSequence && token.endsWith("]"))) {
                isBracketSequence = false;
            }
            if (!isBracketSequence && (!token.startsWith("(") && !token.endsWith(")")) &&
                    (!token.startsWith("[") && !token.endsWith("]"))) {
                stringBuilder.append(token);
                stringBuilder.append(" ");
            }
        }
        String filteredSentence = stringBuilder.toString();
        List<String> tokens = tokenizer.splitStringIntoList(filteredSentence);
        for (String token : tokens) {
            if (token.contains(String.valueOf('[')) || token.contains(String.valueOf(']'))) {
                token = tokenizer.removeBrackets(token, '[', ']');
            }
            if (token.contains(String.valueOf('(')) || token.contains(String.valueOf(')'))) {
                token = tokenizer.removeBrackets(token, '(', ')');
            }

            token = tokenizer.removeSpecialCharacters(token);
            preprocessedTokens.add(token);
        }

        final List<String> processedTokens = removeEmptyStringInSentence(preprocessedTokens);
        return convertListToString(processedTokens);
    }

    private List<String> removeEmptyStringInSentence(List<String> filteredTokens) {
        final List<String> listTokens = new ArrayList<String>();
        for (final String token : filteredTokens) {
            if (!token.equals("")) {
                listTokens.add(token);
            }
        }
        return listTokens;
    }

    private String convertListToString(List<String> list) {
        String newString = "";
        int i = 0;
        for (String word : list) {
            if (i < list.size() - 1) {
                newString += word + " ";
            } else {
                newString += word;
            }
            i++;
        }
        return newString;
    }
}
