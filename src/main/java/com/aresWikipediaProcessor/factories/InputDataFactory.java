package com.aresWikipediaProcessor.factories;

import com.aresWikipediaProcessor.data.InputData;

import java.util.List;

public interface InputDataFactory {

    InputData create(String sentence, List<String> tagSequencesList);

}