package com.aresWikipediaProcessor.factories;

import com.aresWikipediaProcessor.data.InputData;
import com.aresWikipediaProcessor.tokenizing.Tokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

@Component
public class InputDataFactoryImpl implements InputDataFactory {

    private final static Logger LOGGER = Logger.getLogger(InputDataFactoryImpl.class.getName());

    @Autowired
    private Tokenizer tokenizer;

    @Override
    public InputData create(String sentence, List<String> tagSequencesList) {
        //LOGGER.info("ENTERING create method of InputDataFactoryImpl... ");
        //LOGGER.info("*********************************************************************");

        InputData inputData = new InputData();

        //LOGGER.info("Processing sentence < " + sentence);

        List<String> tokensList = tokenizer.splitStringIntoList(sentence);

        //LOGGER.info("Sentence does not contain any subSentences.");

        inputData.setTokensList(tokensList);
        List<String> tagsList = tokenizer.splitStringIntoList(tagSequencesList.get(0));
        inputData.setTagsList(tagsList);


        //LOGGER.info("LEAVING create method of SubPathDataListFactoryImpl... ");
        //LOGGER.info("*********************************************************************");

        return inputData;
    }

}
