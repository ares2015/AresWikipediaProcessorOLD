package com.aresWikipediaProcessor.morphology;

public interface NumberPrefixDetector {

    boolean detect(String token);

}