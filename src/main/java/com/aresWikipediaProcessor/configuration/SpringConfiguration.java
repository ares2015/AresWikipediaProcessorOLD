package com.aresWikipediaProcessor.configuration;

import com.aresWikipediaProcessor.database.DatabaseAccessor;
import com.aresWikipediaProcessor.factories.InputDataFactory;
import com.aresWikipediaProcessor.preprocessing.CapitalizedTokensPreprocessor;
import com.aresWikipediaProcessor.preprocessing.SentencesPreprocessor;
import com.aresWikipediaProcessor.preprocessing.SubSentencesProcessor;
import com.aresWikipediaProcessor.tagger.StanfordPosTagger;
import com.aresWikipediaProcessor.tokenizing.Tokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component("springConfiguration")
@ComponentScan(basePackages = "com.aresWikipediaProcessor")
public class SpringConfiguration {

    @Autowired
    private DatabaseAccessor databaseAccessor;

    @Autowired
    private Tokenizer tokenizer;

    @Autowired
    private InputDataFactory inputDataFactory;

    @Autowired
    private CapitalizedTokensPreprocessor capitalizedTokensPreprocessor;

    @Autowired
    private StanfordPosTagger stanfordPosTagger;

    @Autowired
    private SentencesPreprocessor sentencesPreprocessor;

    @Autowired
    private SubSentencesProcessor subSentencesProcessor;

    @Bean
    public DriverManagerDataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://mysql15.hostmaster.sk/do784507db");
        dataSource.setUsername("do784500");
        dataSource.setPassword("glavoika");
        return dataSource;
    }

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate(DataSource dataSource) {
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    @Bean
    public JdbcTemplate standardJdbcTemplate(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate;
    }

    public DatabaseAccessor getDatabaseAccessor() {
        return databaseAccessor;
    }

    public Tokenizer getTokenizer() {
        return tokenizer;
    }

    public InputDataFactory getInputDataFactory() {
        return inputDataFactory;
    }

    public CapitalizedTokensPreprocessor getCapitalizedTokensPreprocessor() {
        return capitalizedTokensPreprocessor;
    }

    public StanfordPosTagger getStanfordPosTagger() {
        return stanfordPosTagger;
    }

    public SentencesPreprocessor getSentencesPreprocessor() {
        return sentencesPreprocessor;
    }

    public SubSentencesProcessor getSubSentencesProcessor() {
        return subSentencesProcessor;
    }
}
